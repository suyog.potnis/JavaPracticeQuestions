import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/*
  @author:Spotnis
  Date: 09/01/2017
 */
public class StringInterviewQuestions {

	public static void main(String[] args) {

	}
	
//1. Check if Strings are Isomorphic
//	 Eg: add and egg 
		public static String isIsomorphic(String s, String t) {
		    if(s==null||t==null)
		        {System.out.print("False");
		        return "false";
		        }
		    
		 
		    if(s.length()!=t.length()){
		    	 System.out.print("False");
		    	 return "false";
		    }
		    HashMap<Character, Character> map = new HashMap<Character, Character>();
		 
		 
		    for(int i=0; i<s.length(); i++){
		        char c1 = s.charAt(i);
		        char c2 = t.charAt(i);
		 
		        if(map.containsKey(c1)){
		            if(map.get(c1)!=c2)// if not consistant with previous ones
		            	 System.out.print("False");
		            return "false";
		        }else{
		            if(map.containsValue(c2)) {//if c2 is already being mapped
		            	 System.out.print("False");
		            	 return "false";
		            }
		            map.put(c1, c2);
		        }
		    }
		 
		    System.out.print("True");
		    return "True";
		}
	

	}
