
public class IntegerArrayInterviewQuestions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
//1. Return first Duplicate 	
static int firstDuplicate(int[] a) {
		HashMap<Integer,Integer> myhash = new HashMap<Integer,Integer>();
		    for(int i =0;i<a.length;i++)
		    {
		        if(myhash.isEmpty())
		            myhash.put(a[i],1);
		        else{
		            if(myhash.containsKey(a[i]))
		            	return a[i];
		            else
		            {
		            	myhash.put(a[i], 1);
		            }
		        }
		    }
			return 0;
		}

}
